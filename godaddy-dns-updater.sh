#!/bin/bash

script_name_full="$(basename ${0})"
script_name_base="${script_name_full%%.*}"
script_file_full="$(realpath -s ${0})"
script_file_base="${script_file_full%%.*}"
config_file_full="${script_file_base}.conf"

log_file_enabled=0

#################################### Start of functions ############################################

function log_prefix() {
    echo "$(date +%F" "%T) $(hostname)"
}

function log_info() {
    local prefix="$(log_prefix)"
    echo "${prefix} INFO  ${1}"
    [ "${log_file_enabled}" -eq 1 ] && echo "${prefix} INFO  ${1}" >> "${log_file}"
}

function log_error() {
    local prefix="$(log_prefix)"
    echo "${prefix} ERROR ${1}"
    [ "${log_file_enabled}" -eq 1 ] && echo "${prefix} ERROR ${1}" >> "${log_file}"
}

function load_config() {
    log_info "Configuration file: '${config_file_full}'"
    if [ ! -f "${config_file_full}" ]; then
        log_error "Configuration file '${config_file_full}' does not exist"
        return 1
    fi
    source "${config_file_full}"
}

function check_config() {
    if [ -z "${log_file}" ] || [ ! -w "${log_file}" ]; then
        log_error "Log file '${log_file}' cannot be opened for writing."
        return 1
    fi

    if [ -z "${domain_name}" ]; then
        log_error "Domain name empty."
        return 1
    fi

    if [ -z "${domain_record_type}" ]; then
        log_error "Domain record type empty."
        return 1
    fi

    if [ -z "${domain_record_name}" ]; then
        log_error "Domain record name empty."
        return 1
    fi

    if [ -z "${domain_record_ttl}" ] || [[ ! "${domain_record_ttl}" =~ ^[0-9]+$ ]]; then
        log_error "Domain record TTL invalid."
        return 1
    fi

    if [ -z "${godaddy_key}" ]; then
        log_error "Godady API key empty."
        return 1
    fi

    if [ -z "${godaddy_secret}" ]; then
        log_error "Godady API secret empty."
        return 1
    fi
}

##################################### End of functions #############################################

load_config
if [ "$?" -ne 0 ]; then
    exit 1
fi

check_config
if [ "$?" -ne 0 ]; then
    exit 1
fi

log_file_enabled=1

log_info "---- Godady DNS Updater ----"

auth_headers="Authorization: sso-key ${godaddy_key}:${godaddy_secret}"

result=$(curl -s -X GET \
    -H "${auth_headers}" \
    "https://api.godaddy.com/v1/domains/${domain_name}/records/${domain_record_type}/${domain_record_name}")

if [ "$?" -ne 0 ]; then
    log_error "Cannot get current record."
    exit 1
fi

dns_ip=$(echo ${result} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
log_info "Current DNS IP: ${dns_ip}"

result=$(dig +short myip.opendns.com @resolver1.opendns.com)

if [ "$?" -ne 0 ]; then
    log_error "Cannot get current IP."
    exit 1
fi

current_ip=$(echo ${result} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
log_info "Current IP: ${current_ip}"

if [ "${dns_ip}" = "${current_ip}" ]; then
    log_info "DNS update not required."
    exit 0
fi

result=$(curl -s -X PUT \
    "https://api.godaddy.com/v1/domains/${domain_name}/records/${domain_record_type}/${domain_record_name}" \
    -H "accept: application/json" \
    -H "Content-Type: application/json" \
    -H "${auth_headers}" \
    -d "[ { \"data\": \"${current_ip}\", \"ttl\": ${domain_record_ttl} } ]")

if [ "$?" -ne 0 ]; then
    log_error "Cannot update record."
    exit 1
fi

if [ ! -z "${result}" ]; then
    log_error "Cannot update record. ${result}"
    exit 1
fi

log_info "DNS updated."